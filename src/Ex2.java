import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ex2 {
    public static void main(String[] args) {
        new Win();
    }
}

class Win extends JFrame {
    JTextField inm1;
    JTextField inm2;
    JTextField rez;
    JButton doButton;
    public Win(){
        this.setSize(300,300);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(null);

        inm1 = new JTextField();
        inm1.setBounds(10,10,150,30);
        this.add(inm1);

        inm2 = new JTextField();
        inm2.setBounds(10,50,150,30);
        this.add(inm2);

        doButton = new JButton("Calculate");
        doButton.setBounds(10,120,150,30);
        this.add(doButton);


        rez = new JTextField();
        rez.setBounds(10,160,150,30);
        rez.setEditable(false);
        this.add(rez);

        doButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rez.setText(Integer.parseInt(inm1.getText())*Integer.parseInt(inm2.getText())+"");
            }
        });

        this.setVisible(true);
    }
}
